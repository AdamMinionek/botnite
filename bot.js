// BotNite-JS
// """rewritten""" in JS using a lot of code from HiddenPhox

// Require various things and creates the bot.
const Eris = require("eris");
const fs = require("fs");
const superagent = require("superagent");
const config = require("./config.json");
const bot = new Eris(config.general.token);

// Initialize context. Bless Cyn for making HF open source I swear.
const ctx = {};
ctx.cmds = new Eris.Collection();
ctx.events = new Eris.Collection();
ctx.bot = bot;
ctx.utils = require("./utils.js");
ctx.libs = {
  eris: Eris,
  superagent: superagent,
};

// Add config stuff.
ctx.prefix = config.general.prefix;
ctx.ownerid = config.general.ownerId;
ctx.logid = config.admin.logid;
ctx.skyblockToken = config.admin.skyblock;
ctx.name = config.general.name;
ctx.failPerm = config.general.failPerm;
ctx.elevated = config.admin.elevated;

// Logs the bot is ready.
bot.on("ready", () => {
  console.log(ctx.name + " loaded.");
  console.log(
    `Bot loaded as: ${bot.user.username}#${bot.user.discriminator} / ${bot.user.id}`
  );
});

// I don't understand a single line of code below this comment.
var files = fs.readdirSync("./cmds/");
for (let f of files) {
  let c = require(__dirname + "/cmds/" + f);

  if (c.name && c.func) {
    ctx.cmds.set(c.name, c);
    console.log(`Loaded Command: ${c.name}`);
  } else if (c.length) {
    for (let i = 0; i < c.length; i++) {
      let a = c[i];
      if (a.func && a.name) {
        ctx.cmds.set(a.name, a);
        console.log(`Loaded Command: ${a.name} (${f})`);
      }
    }
  }
}

let createEvent = function (bot, e, ctx) {
  if (e.event == "timer") {
    if (!e.interval) {
      console.log(
        `No interval for event: ${
          e.event + "|" + e.name
        }, not setting up interval.`
      );
      return;
    }
    ctx.events.get(e.event + "|" + e.name).timer = setInterval(
      e.func,
      e.interval,
      ctx
    );
  } else {
    bot.on(e.event, (...args) => e.func(...args, ctx));
  }
};

var files = fs.readdirSync(__dirname + "/events");
for (let f of files) {
  let e = require(__dirname + "/events/" + f);
  if (e.event && e.func && e.name) {
    ctx.events.set(e.event + "|" + e.name, e);
    createEvent(bot, e, ctx);
    console.log(`Loaded event: ${e.event}|${e.name} (${f})`);
  } else if (e.length) {
    for (let i = 0; i < e.length; i++) {
      let a = e[i];
      if (a.event && a.func && a.name) {
        ctx.events.set(a.event + "|" + a.name, a);
        createEvent(bot, a, ctx);
        console.log(`Loaded event: ${a.event}|${a.name} (${f})`);
      }
    }
  }
}

async function commandHandler(msg) {
  if (msg.author) {
    if (msg.author.id == ctx.bot.user.id) return;

    let prefix = ctx.prefix;
    let prefix2 = ctx.bot.user.mention + " ";
    let hasRan = false;
    let content = msg.content;

    if (content.startsWith(prefix2)) {
      content = content.replace(prefix2, prefix);
    }

    let [cmd, ...args] = content.split(" ");

    let [cmd2, ...args2] = msg.cleanContent.split(" ");

    cmd = cmd.toLowerCase();
    cmd2 = cmd2.toLowerCase();

    for (const c of ctx.cmds.values()) {
      if (cmd == prefix + c.name) {
        if (
          c.guild &&
          msg.channel.guild &&
          !c.guild.includes(msg.channel.guild.id)
        )
          return;
        try {
          c.func(ctx, msg, args.join(" "));
          ctx.utils.logInfo(
            ctx,
            `'${msg.author.username}' (${msg.author.id}) ran command '${cmd2} ${
              cmd2 == prefix + "eval"
                ? "<eval redacted>"
                : args2.join(" ").split("").splice(0, 50).join("")
            }${args2.join(" ").length > 50 ? "..." : ""}' in '#${
              msg.channel.name ? msg.channel.name : msg.channel.id
            }' on '${msg.channel.guild ? msg.channel.guild.name : "DMs"}'${
              msg.channel.guild ? " (" + msg.channel.guild.id + ")" : ""
            }`
          );
        } catch (e) {
          msg.channel.createMessage(
            ":warning: An error occured.\n```\n" + e.message + "\n```"
          );
          ctx.utils.logWarn(
            ctx,
            `'${cmd2} ${
              cmd2 == prefix + "eval"
                ? "<eval redacted>"
                : args2.join(" ").split("").splice(0, 50).join("")
            }${args2.join(" ").length > 50 ? "..." : ""}' errored with '${
              e.message
            }'`
          );
        }

        hasRan = true;
        msg.hasRan = true;
      }

      if (
        c.aliases &&
        (cmd == prefix + c.name ||
          cmd ==
            prefix + c.aliases.find((a) => a == cmd.replace(prefix, ""))) &&
        !hasRan
      ) {
        if (
          c.guild &&
          msg.channel.guild &&
          !c.guild.includes(msg.channel.guild.id)
        )
          return;
        try {
          c.func(ctx, msg, args.join(" "));
          ctx.utils.logInfo(
            ctx,
            `'${msg.author.username}' (${
              msg.author.id
            }) ran guild command '${cmd2} ${
              cmd2 == prefix + "eval"
                ? "<eval redacted>"
                : args2.join(" ").split("").splice(0, 50).join("")
            }${args2.join(" ").length > 50 ? "..." : ""}' in '#${
              msg.channel.name ? msg.channel.name : msg.channel.id
            }' on '${msg.channel.guild ? msg.channel.guild.name : "DMs"}'${
              msg.channel.guild ? " (" + msg.channel.guild.id + ")" : ""
            }`
          );
        } catch (e) {
          msg.channel.createMessage(
            ":warning: An error occured.\n```\n" + e.message + "\n```"
          );
          ctx.utils.logWarn(
            ctx,
            `'${cmd2} ${
              cmd2 == prefix + "eval"
                ? "<eval redacted>"
                : args2.join(" ").split("").splice(0, 50).join("")
            }${args2.join(" ").length > 50 ? "..." : ""}' errored with '${
              e.message
            }'`
          );
        }

        hasRan = true;
        msg.hasRan = true;
      }
    }
  }
}

bot.on("messageCreate", commandHandler);

bot.on("messageUpdate", (msg) => {
  let oneday = Date.now() - 86400000;
  if (msg.timestamp > oneday && !msg.hasRan) {
    commandHandler(msg);
  }
});

bot.connect(); // Keep this at the bottom, you cretin.
