let v3 = async function(ctx, msg, args) {
    let responses = [
        "soon",
        "Tuesday",
        "May 16, 2020",
        "3 minutes",
        "blease luna robogirl when is v3??????",
        "when it's ready",
        "\"\"\"\"\"soon\"\"\"\"\"",
        "after this",
        "Now. V3 Is Real.",
        "when Rover wakes up",
        "probably soon",
        "idk",
        "can't answer that question",
        "Blease. V3 When ?? ? ? ? I Want Use New Feature",
        "when antichrist is finished",
        "soon\u2122",
        "as soon as this joke dies out",
        "sometime",
        "next Saturday",
        "write it yourself",
        "\"it's python how bad can it be\"",
        "in 20 minutes",
        "probably later this year",
        "4:00 PM PDT",
        "When your stupid ass contributes. <https://gitlab.com/elixire/elixire/-/milestones>",
        "good fucking question",
        "atluna can you answer this for me?",
        "August 19, 2020"
    ];
    let reply = responses[Math.floor(Math.random() * responses.length)];
    msg.channel.createMessage(reply);
}

let lights = async function(ctx, msg, args) {
    let a = args.split(" ")
    if (a.length !== 3) {
        console.log(a.length)
        msg.channel.createMessage("Usage: ,lights R G B");
    }
    ctx.libs.superagent.get(`https://soulja-boy-told.me/light?r=${a[0]}&g=${a[1]}&b=${a[2]}&bri=255&on=true`)
    .set('User-Agent', `BotNite ${msg.author.username}#${msg.author.discriminator}`)
    .then(res => {}) // lol
}

module.exports = [
    {
        name: "v3wen",
        desc: "v3 wen plz luna robogirl",
        func: v3,
        group: "fun"
    },
    {
        name: "lights",
        desc: "Turn on/off Julian's lights",
        func: lights,
        group: "fun"
    }
]