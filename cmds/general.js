let erisv = require("../node_modules/eris/package.json").version; // Shh.

let help = async function(ctx, msg, args) {
  const sorted = {};
  for (const cmd of ctx.cmds.values()) {
      if (sorted[cmd.group.toLowerCase()] === undefined) {
          sorted[cmd.group.toLowerCase()] = [];
      }
      sorted[cmd.group.toLowerCase()].push(cmd);
  }

  if (args) {
      if (args.startsWith("--")) {
          const category = args
              .replace("--", "")
              .toLowerCase()
              .trim();

          if (sorted[category]) {
              const embed = {
                  embed: {
                      title: `${ctx.name} Help: ${category
                          .toUpperCase()
                          .charAt(0) + category.toLowerCase().slice(1)}`,
                      color: ctx.utils.topColor(
                          ctx,
                          msg,
                          ctx.bot.user.id,
                          0x8060c0
                      ),
                      fields: []
                  }
              };
              for (const cmd of sorted[category]) {
                  embed.embed.fields.push({
                      name: `${ctx.prefix}${cmd.name}`,
                      value: cmd.desc,
                      inline: true
                  });
              }
              msg.channel.createMessage(embed);
          } else {
              msg.channel.createMessage("Category not found.");
          }
      } else {
          if (
              ctx.cmds.filter(
                  c =>
                      c.name == args ||
                      (c.aliases && c.aliases.includes(args))
              ).length > 0
          ) {
              const cmd = ctx.cmds.filter(
                  c =>
                      c.name == args ||
                      (c.aliases && c.aliases.includes(args))
              )[0];
              const embed = {
                  title: `${ctx.name} Help: \`${ctx.prefix}${cmd.name}\``,
                  color: ctx.utils.topColor(
                      ctx,
                      msg,
                      ctx.bot.user.id,
                      0x8060c0
                  ),
                  description: cmd.desc || "This command doesn't have a description.",
                  fields: [
                      { name: "Category", value: cmd.group, inline: true }
                  ]
              };

              if (cmd.usage) {
                  embed.fields.push({
                      name: "Usage",
                      value: `${ctx.prefix}${cmd.name} ${cmd.usage}`,
                      inline: true
                  });
              }
              if (cmd.aliases) {
                  embed.fields.push({
                      name: "Aliases",
                      value: cmd.aliases.join(", "),
                      inline: true
                  });
              }
              msg.channel.createMessage({ embed: embed });
          } else {
              msg.channel.createMessage("Command not found.");
          }
      }
  } else {
      const embed = {
          embed: {
              title: `${ctx.name} Help`,
              color: ctx.utils.topColor(ctx, msg, ctx.bot.user.id, 0x8060c0),
              fields: []
          }
      };
      for (const x in sorted) {
          embed.embed.fields.push({
              name: x.toUpperCase().charAt(0) + x.toLowerCase().slice(1),
              value: `${sorted[x].length} Commands\n${
                  ctx.prefix
              }help --${x.toLowerCase()}`,
              inline: true
          });
      }
      msg.channel.createMessage(embed);
  }
};

let info = async function(ctx, msg, args) {
    msg.channel.createMessage({
    embed: {
      title: ctx.name,
      description: `Written by **NotNite** \`NotNite#0001\`.`,
      color: 0x50596d,
      fields: [
        { name: "Language", value: "JavaScript", inline: true },
        { name: "Library", value: `Eris v${erisv}`, inline: true },
        {
          name: "Node.js Version",
          value: process.version,
          inline: true
        },
        {
          name: "Honorable Mentions",
          value: `**Cynosphere** - Developer of HiddenPhox and help when I'm fungus\n**Xbox Live Party** - Side-developers and crazed code friends\n**elixi.re** - Inspiration to continue this terrible bot\n**TSHNN** - The only server that can utilize this bot well lmao`
        },
        {
          name: "Links",
          value: "[Source](https://gitlab.com/xboxliveparty/botnite)"
        }
      ]
    }
  });
};

module.exports = [
    {
        name: "info",
        desc: "Shows generic bot info.",
        func: info,
        group: "general"
    },
    {
      name: "help",
      desc: "Shows command information",
      func: help,
      group: "general"
    }
]