const hasteRegex = /^(https?:\/\/)?(www\.)?(hastebin\.com|mystb\.in)\/(raw\/)?([a-z]+)(\.[a-z]+)?$/;
const jimp = require("jimp");

let _eval = async function (ctx, msg, args) {
  if (msg.author.id === ctx.ownerid || ctx.elevated.includes(msg.author.id)) {
    let errored = false;
    let out;
    let isURL = false;

    if (hasteRegex.test(args)) {
      let url = args.match(hasteRegex);
      args = `${url[1]}${url[3]}/raw/${url[5]}`;
      isURL = true;
    }

    if (isURL) {
      let toRun = await ctx.libs.superagent.get(args).then((x) => x.text);

      try {
        out = eval(toRun);
        if (out && out.then) out = await out;
      } catch (e) {
        out = e.message ? e.message : e;
        errored = true;
      }
    } else {
      try {
        out = eval(args);
        if (out && out.then) out = await out;
      } catch (e) {
        out = e.message ? e.message : e;
        errored = true;
      }
    }

    out =
      typeof out == "string" ? out : require("util").inspect(out, { depth: 0 });

    let token = ctx.bot.token;
    out = out.replace(
      new RegExp(token.replace(/\./g, "\\."), "g"),
      "No Key 4 U Dipshit"
    );

    if (errored) {
      msg.channel.createMessage(
        ":warning: Output (errored):\n```js\n" + out + "\n```"
      );
    } else {
      if (out.toString().length > 1980) {
        let output = out.toString();
        ctx.utils.makeHaste(
          ctx,
          msg,
          output,
          "\u2705 Output too long to send in a message: "
        );
      } else {
        msg.channel.createMessage("\u2705 Output:\n```js\n" + out + "\n```");
      }
    }
  } else {
    msg.channel.createMessage(ctx.failPerm);
  }
};

let exec = function (ctx, msg, args) {
  if (msg.author.id === ctx.ownerid || ctx.elevated.includes(msg.author.id)) {
    args = args.replace(/rm \-rf/g, "echo");
    require("child_process").exec(args, (e, out, err) => {
      if (e) {
        msg.channel.createMessage("Error\n```" + e + "```");
      } else {
        if (out.toString().length > 1980) {
          let output = out.toString();
          ctx.utils.makeHaste(
            ctx,
            msg,
            output,
            "\u2705 Output too long to send in a message: "
          );
        } else {
          msg.channel.createMessage(
            "\u2705 Output:\n```bash\n" + out + "\n```"
          );
        }
      }
    });
  } else {
    msg.channel.createMessage(ctx.failPerm);
  }
};

let restart = function (ctx, msg, args) {
  if (msg.author.id === ctx.ownerid || ctx.elevated.includes(msg.author.id)) {
    msg.channel.createMessage(`Restarting ${ctx.name}...`);
    setTimeout(process.exit, 500);
  } else {
    msg.channel.createMessage(ctx.failPerm);
  }
};

let mask = async function (ctx, msg, args) {
  if (msg.author.id === ctx.ownerid) {
    let av = await ctx.utils
      .lookupUser(ctx, msg, msg.author.mention)
      .then((u) => {
        return `https://cdn.discordapp.com/avatars/${u.id}/${u.avatar}.${
          u.avatar.startsWith("a_") ? "gif?size=1024&_=.gif" : "png?size=1024"
        }`;
      });
    let img = await jimp.read(av);
    let template = await jimp.read(`${__dirname}/../res/avatar.png`);
    img.resize(1024, 1024);
    img.invert();
    img.composite(template, 0, 0)
    img.getBase64(jimp.AUTO, (err, res) => {
      let output = res.toString();
      let options = {
          avatar: output
      }
      ctx.bot.editSelf(options);
    });
  } else {
    msg.channel.createMessage(ctx.failPerm);
  }
};

module.exports = [
  {
    name: "eval",
    desc: "Evaluates JS code",
    func: _eval,
    usage: "<string>",
    group: "admin",
  },
  {
    name: "exec",
    desc: "Runs code in shell",
    func: exec,
    usage: "<command>",
    group: "admin",
  },
  {
    name: "restart",
    desc: "Restarts the bot",
    func: restart,
    group: "admin",
  },
  {
    name: "mask",
    desc: "Become Julian",
    func: mask,
    group: "admin",
  },
];
