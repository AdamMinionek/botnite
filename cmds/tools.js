const superagent = require("superagent");
const moment = require("moment-timezone");

let icon = function (ctx, msg, args) {
  if (args && (args == "--server" || args == "--guild")) {
    msg.channel.createMessage({
      embed: {
        title: `Server icon`,
        image: {
          url: `https://cdn.discordapp.com/icons/${msg.channel.guild.id}/${
            msg.channel.guild.icon
          }.${
            msg.channel.guild.icon.startsWith("a_")
              ? "gif?size=1024&_=.gif"
              : "png?size=1024"
          }`,
        },
      },
    });
  } else {
    ctx.utils
      .lookupUser(ctx, msg, args ? args : msg.author.mention)
      .then((u) => {
        let av = `https://cdn.discordapp.com/avatars/${u.id}/${u.avatar}.${
          u.avatar.startsWith("a_") ? "gif?size=1024&_=.gif" : "png?size=1024"
        }`;
        msg.channel.createMessage({
          embed: {
            title: `Avatar for **${u.username}#${u.discriminator}**`,
            image: {
              url: av,
            },
          },
        });
      });
  }
};

let skyblock = async function (ctx, msg, args) {
  const mcRegex = /[a-zA-Z0-9_]{1,16}/g;
  args = ctx.utils.formatArgs(args);
  if (args.length < 1 || args.length > 2) {
    msg.channel.createMessage(
      `Too little/many arguments. You can get your profiles with ${ctx.prefix}skyblock <username> and details on your profile with ${ctx.prefix}skyblock <username> <profile>.\nNote that profile names are case sensitive, and you must have all API settings on.`
    );
    return;
  }
  var usernameTest = mcRegex.test(args[0]);
  if (usernameTest == true) {
    var username = args[0];
    var profileCuteName = args[1];
    let usernameData = await superagent
      .get(`https://api.mojang.com/users/profiles/minecraft/${username}`)
      .set(
        "User-Agent",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
      )
      .then((x) => x.body)
      .catch((e) =>
        msg.channel.createMessage(
          ":warning: An error occured.\n```\n" + e.message + "\n```"
        )
      );
    if (usernameData.id == undefined) {
      msg.channel.createMessage(
        `Either you entered the wrong name, or that name didn't seem to work. Try again?`
      );
      return;
    }
    var uuid = usernameData.id;
    let hypixelData = await superagent
      .get(
        `https://api.hypixel.net/player?uuid=${uuid}&key=${ctx.skyblockToken}`
      )
      .set(
        "User-Agent",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
      )
      .then((x) => x.body)
      .catch((e) =>
        msg.channel.createMessage("Couldn't find your SkyBlock data.")
      );
    if (!hypixelData) {
      msg.channel.createMessage("Couldn't find your SkyBlock data.");
      return;
    }
    if (!hypixelData.player) {
      msg.channel.createMessage("Couldn't find your SkyBlock data.");
      return;
    }
    let skyblockProfiles = hypixelData.player.stats.SkyBlock.profiles;
    if (!profileCuteName) {
      msg.channel.createMessage({
        embed: {
          title: `Profiles for **${username}**`,
          color: 0x76b04a,
          thumbnail: { url: `https://visage.surgeplay.com/face/${uuid}.png` },
          footer: { text: "Powered by visage.surgeplay.com" },
          fields: [
            {
              name: "Profiles",
              value: `\n- ${Object.values(skyblockProfiles)
                .map((x) => x.cute_name)
                .join("\n- ")}`,
            },
            {
              name: "Links",
              value: `[sky.lea.moe](https://sky.lea.moe/stats/${username})`,
            },
          ],
        },
      });
      return;
    }
    let profile = Object.values(skyblockProfiles).filter(
      (x) => x.cute_name.toLowerCase() == profileCuteName.toLowerCase()
    )[0];
    if (!profile) {
      msg.channel.createMessage(`Couldn't find your SkyBlock profile.`);
      return;
    }

    let profileID = profile.profile_id;
    let profileData = await superagent
      .get(
        `https://api.hypixel.net/skyblock/profile?profile=${profileID}&key=${ctx.skyblockToken}`
      )
      .set(
        "User-Agent",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
      )
      .then((x) => x.body)
      .catch((e) =>
        msg.channel.createMessage("Couldn't find your SkyBlock data.")
      );
    if (!profileData) return;

    let userSpecificProfile = profileData.profile.members[uuid];
    try {
      msg.channel.createMessage({
        embed: {
          title: `${username} @ ${profileCuteName}`,
          description: `${
            profileData.profile.banking.balance
              ? profileData.profile.banking.balance.toFixed(2)
              : 0
          } Coins - ${
            Object.keys(profileData.profile.members).length
          }/5 Members`,
          color: 0x76b04a,
          thumbnail: { url: `https://visage.surgeplay.com/face/${uuid}.png` },
          footer: { text: "Powered by visage.surgeplay.com" },
          fields: [
            {
              name: "Information",
              value: `**${
                userSpecificProfile.coin_purse
                  ? userSpecificProfile.coin_purse.toFixed(2)
                  : 0
              }** Coins in Purse\n**${
                userSpecificProfile.stats.kills
                  ? userSpecificProfile.stats.kills
                  : 0
              }** Kills\n**${
                userSpecificProfile.stats.deaths
                  ? userSpecificProfile.stats.deaths
                  : 0
              }** Deaths\n**${
                userSpecificProfile.stats.auctions_created
                  ? userSpecificProfile.stats.auctions_created
                  : 0
              }** Auctions Created\n**${
                userSpecificProfile.stats.auctions_won
                  ? userSpecificProfile.stats.auctions_won
                  : 0
              }/${
                userSpecificProfile.stats.auctions_bids
                  ? userSpecificProfile.stats.auctions_bids
                  : 0
              }** Auctions Won\n**${
                userSpecificProfile.fairy_souls_collected
                  ? userSpecificProfile.fairy_souls_collected
                  : 0
              }** Fairy Souls Collected`,
            },
            {
              name: "Links",
              value: `[sky.lea.moe](https://sky.lea.moe/stats/${username}/${profileCuteName})`,
            },
          ],
        },
      });
    } catch {
      msg.channel.createMessage(
        "Couldn't find your SkyBlock data. You might not have full API access enabled."
      );
      return;
    }
  }
};

let album = function (ctx, msg, args) {
  args = ctx.utils.formatArgs(args);
  var regex = /(https:\/\/)([a-z0-9-.]+)(\/i\/)([a-z0-9]{3,8}\.png|jpg)/s;
  if (args.length < 2) {
    msg.channel.createMessage(
      "why are you trying to make an album link with only one link lol"
    );
    return;
  }
  var found = [];
  for (i = 0; i < args.length; i++) {
    if (args[i].match(regex)) {
      if (args[i].match(regex)[2] != args[0].match(regex)[2]) {
        msg.channel.createMessage("There seems to be a domain mismatch.");
        return;
      }
      var file = args[i].match(regex);
      found.push(file[4]);
    } else {
      msg.channel.createMessage("Those don't seem to be valid elixi.re links.");
      return;
    }
  }
  var domain = args[0].match(regex)[2];
  msg.channel.createMessage(
    `https://elixi.re/album.html#${domain};${found.join(",")}`
  );
};

module.exports = [
  {
    name: "icon",
    desc: "Gets avatar or server icon",
    func: icon,
    usage: "<user id/mention> or --server or --guild",
    group: "tools",
  },
  {
    name: "skyblock",
    desc: "Gets information on a Hypixel Skyblock user",
    func: skyblock,
    usage: "<minecraft username> or <minecraft username> <profile>",
    group: "tools",
  },
  {
    name: "album",
    desc: "Generates elixi.re album links",
    func: album,
    usage: "2 or more elixi.re links",
    group: "tools",
  },
];
