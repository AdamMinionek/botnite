const jimp = require("jimp");
const urlRegex = /((http[s]?):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+)/;
const fs = require("fs");

async function imageCallback(ctx, msg, args, callback, ...cbargs) {
  msg.channel.sendTyping();

  if (args && urlRegex.test(args)) {
    let filename,
      out = await callback.apply(this, [msg, args, ...cbargs]);
    if (filename && out)
      msg.channel.createMessage("", {
        name: filename,
        file: out,
      });
  } else if (msg.attachments.length > 0) {
    let filename,
      out = await callback.apply(this, [
        msg,
        msg.attachments[0].url,
        ...cbargs,
      ]);
    if (filename && out)
      msg.channel.createMessage("", {
        name: filename,
        file: out,
      });
  } else if (/[0-9]{17,21}/.test(args)) {
    let u = await ctx.utils.lookupUser(ctx, msg, args);
    let url =
      u.avatar !== null
        ? `https://cdn.discordapp.com/avatars/${u.id}/${u.avatar}.${
            u.avatar.startsWith("a_") ? "gif" : "png"
          }?size=1024`
        : `https://cdn.discordapp.com/embed/avatars/${u.discriminator % 5}.png`;
    let filename,
      out = await callback.apply(this, [msg, url, ...cbargs]);
    if (filename && out)
      msg.channel.createMessage("", {
        name: filename,
        file: out,
      });
  } else {
    try {
      let img = await ctx.utils.findLastImage(ctx, msg);
      let filename,
        out = await callback.apply(this, [msg, img, ...cbargs]);
      if (filename && out)
        msg.channel.createMessage("", {
          name: filename,
          file: out,
        });
    } catch (e) {
      msg.channel.createMessage(
        "Image not found. Please give URL, attachment or user mention."
      );
    }
  }
}

let _rover = async function (msg, url) {
  let template = await jimp.read(`${__dirname}/../res/rover.png`);
  let img = await jimp.read(url);
  let out = new jimp(template.bitmap.width, template.bitmap.height, 0);
  img.resize(192, 102);
  img.rotate(-2.4, false);
  out.composite(img, 60, 125);
  out.composite(template, 0, 0);

  let toSend = await out.getBufferAsync(jimp.MIME_PNG);
  msg.channel.createMessage("", { file: toSend, name: "rover.png" });
};

let rover = (ctx, msg, args) => imageCallback(ctx, msg, args, _rover);

let drake = async function (ctx, msg, args) {
  let toSend = await fs.readFileSync(`${__dirname}/../res/drake.gif`);
  msg.channel.createMessage("", { file: toSend, name: "drake.gif" });
};

let buncake = async function (ctx, msg, args) {
  let toSend = await fs.readFileSync(`${__dirname}/../res/buncake.png`);
  msg.channel.createMessage("", { file: toSend, name: "buncake.png" });
};

module.exports = [
  {
    name: "rover",
    desc: "Dog hold thing funny",
    func: rover,
    group: "image",
  },
  {
    name: "drake",
    desc: "clap",
    func: drake,
    group: "image",
  },
  {
    name: "buncake",
    desc: "Funy Buny",
    func: buncake,
    group: "image",
  },
];
