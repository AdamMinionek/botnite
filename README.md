# BotNite

BotNite is a fork of [HiddenPhox](https://gitlab.com/Cynosphere/HiddenPhox) intended mainly for the TSHNN discord server.
It is currently in very early stages of development, is quite broken, and is not reccomended for production.
BotNite is licensed under the [MIT license](https://gitlab.com/xboxliveparty/botnite/blob/master/LICENSE.md).
