let statuses = [
  { type: 0, name: "with God" },
  { type: 0, name: "with you \uD83D\uDC40" },
  { type: 0, name: "Mindustry" },
  { type: 3, name: "you sleep" },
  { type: 2, name: "Baby Shark" },
  { type: 2, name: "%ucount% people" },
  { type: 2, name: "%scount% servers" },
  { type: 0, name: "Roblox" },
  { type: 0, name: "Funny Rat Mod" },
  { type: 3, name: "code compile" },
  { type: 3, name: "you type" },
  { type: 0, name: "tshnn.com" },
  { type: 0, name: "'); DROP TABLE Statuses;--" },
  { type: 2, name: "nya.wtf/song" },
  { type: 0, name: "with my cock" },
  { type: 3, name: "%ccount% channels" },
  { type: 0, name: "Monopoly" },
  { type: 3, name: "society" },
];

let setStatus = function (ctx) {
  let status = statuses[Math.floor(Math.random() * statuses.length)];
  status.name = status.name
    .replace("%scount%", ctx.bot.guilds.size)
    .replace("%ucount%", ctx.bot.users.size)
    .replace("%ccount%", Object.keys(ctx.bot.channelGuildMap).length);

  ctx.bot.editStatus("online", {
    type: status.type,
    name: `${status.name} | ${ctx.prefix}help`,
  });
};

module.exports = {
  event: "timer",
  name: "statuses",
  interval: 60000,
  func: setStatus,
};
