const logChannel = "501312043417206804";
const guildId = "501306432260407296";
const roleId = "501310425460375564";

// Green actions, creation of something.
async function tshnnJoin(guild, member, ctx) {
  if (guild.id == guildId) {
    guild.addMemberRole(member.id, roleId, "Joined TSHNN");
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Member Joined**",
        description: `${member.mention} ${member.username}#${member.discriminator}`,
        color: 0x23d160,
        thumbnail: {
          url: `${member.avatarURL}`
        },
        footer: {
          text: `ID: ${member.id}`
        }
      }
    });
  }
}

async function tshnnChannelAdd(channel, ctx) {
  if (channel.guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Channel Created**",
        description: channel.mention,
        color: 0x23d160,
        thumbnail: {
          url: `${channel.guild.iconURL}`
        },
        footer: {
          text: `ID: ${channel.id}`
        }
      }
    });
  }
}

// Red actions, leaving or moderation actions.
async function tshnnLeave(guild, member, ctx) {
  if (guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Member Left**",
        description: `${member.mention} ${member.username}#${member.discriminator}`,
        color: 0xff470f,
        thumbnail: {
          url: `${member.avatarURL}`
        },
        footer: {
          text: `ID: ${member.id}`
        }
      }
    });
  }
}

async function tshnnBan(guild, member, ctx) {
  if (guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Member Banned**",
        description: `${member.mention} ${member.username}#${member.discriminator}`,
        color: 0xff470f,
        thumbnail: {
          url: `${member.avatarURL}`
        },
        footer: {
          text: `ID: ${member.id}`
        }
      }
    });
  }
}

async function tshnnChannelRemove(channel, ctx) {
  if (channel.guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Channel Deleted**",
        description: `#${channel.mention}`,
        color: 0xff470f,
        thumbnail: {
          url: `${channel.guild.iconURL}`
        },
        footer: {
          text: `ID: ${channel.id}`
        }
      }
    });
  }
}

async function tshnnMsgDelete(message, ctx) {
  if (message.channel.guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Message Deleted**",
        description: `${message.channel.mention}`,
        color: 0xff470f,
        thumbnail: {
          url: `${message.channel.guild.iconURL}`
        },
        footer: {
          text: `ID: ${message.id}`
        }
      }
    });
  }
}

// Blue, other.
async function tshnnUnban(guild, member, ctx) {
  if (guild.id == guildId) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Member Unbanned**",
        description: `${member.mention} ${member.username}#${member.discriminator}`,
        color: 0x117ea6,
        thumbnail: {
          url: `${member.avatarURL}`
        },
        footer: {
          text: `ID: ${member.id}`
        }
      }
    });
  }
}

async function tshnnUpdate(guild, member, oldMember, ctx) {
  if (guild.id == guildId) {
    if (member.nick !== oldMember.nick) {
      ctx.bot.createMessage(logChannel, {
        embed: {
          title: "**Nickname Changed**",
          description: `${member.mention} ${member.username}#${member.discriminator}`,
          color: 0x117ea6,
          fields: [
            {
              name: "Before",
              value: oldMember.nick
            },
            {
              name: "After",
              value: member.nick
            }
          ],
          thumbnail: {
            url: `${member.avatarURL}`
          },
          footer: {
            text: `ID: ${member.id}`
          }
        }
      });
    } else if (oldMember.roles.length !== member.roles.length) {
      ctx.bot.createMessage(logChannel, {
        embed: {
          title: "**Member Roles Changed**",
          description: `${member.mention} ${member.username}#${member.discriminator}`,
          color: 0x117ea6,
          fields: [
            {
              name: "Before",
              value: `${oldMember.roles.length} roles`
            },
            {
              name: "After",
              value: `${member.roles.length} roles`
            }
          ],
          thumbnail: {
            url: `${member.avatarURL}`
          },
          footer: {
            text: `ID: ${member.id}`
          }
        }
      });
    }
  }
}

async function tshnnMsgUpdate(message, oldMessage, ctx) {
  if (message.channel.guild.id == guildId && message.content !== oldMessage.content) {
    ctx.bot.createMessage(logChannel, {
      embed: {
        title: "**Message Edited**",
        description: `${message.author.mention} ${message.author.username}#${message.author.discriminator}`,
        color: 0x117ea6,
        fields: [
          {
            name: "Before",
            value: `${oldMessage.content}`
          },
          {
            name: "After",
            value: `${message.content}`
          },
          {
            name: "Channel",
            value: message.channel.mention
          }
        ],
        thumbnail: {
          url: `${message.author.avatarURL}`
        },
        footer: {
          text: `ID: ${message.id}`
        }
      }
    });
  }
}

module.exports = [
  {
    event: "guildMemberAdd",
    name: "TSHNNJoin",
    func: tshnnJoin
  },
  {
    event: "channelCreate",
    name: "TSHNNChannelAdd",
    func: tshnnChannelAdd
  },
  {
    event: "guildMemberRemove",
    name: "TSHNNLeave",
    func: tshnnLeave
  },
  {
    event: "guildBanAdd",
    name: "TSHNNBan",
    func: tshnnBan
  },
  {
    event: "channelDelete",
    name: "TSHNNChannelRemove",
    func: tshnnChannelRemove
  },
  {
    event: "messageDelete",
    name: "TSHNNMsgDelete",
    func: tshnnMsgDelete
  },
  {
    event: "guildBanRemove",
    name: "TSHNNUnban",
    func: tshnnUnban
  },
  {
    event: "guildMemberUpdate",
    name: "TSHNNUpdate",
    func: tshnnUpdate
  },
  {
    event: "messageUpdate",
    name: "TSHNNMsgUpdate",
    func: tshnnMsgUpdate
  }
];
