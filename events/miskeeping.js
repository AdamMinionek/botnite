// TODO: Remove when HiddenPhox comes back up
async function paulBlart(msg, ctx) {
    if (
        msg.channel.id == "625876101826084895" &&
        msg.channel.permissionsOf(ctx.bot.user.id).has("manageMessages")
    ) {
        let m = await msg.channel.getMessages(1, msg.id).then(x => x[0]);
        if (msg.content !== "Paul Blart" || msg.author.id == m.author.id)
            msg.delete().catch(_ => {});
    }
}

async function paulBlart2(msg, omsg, ctx) {
    if (omsg == null) return;
    if (
        msg.channel.id == "625876101826084895" &&
        msg.channel.permissionsOf(ctx.bot.user.id).has("manageMessages")
    ) {
        if (msg.content !== "Paul Blart" && omsg.content == "Paul Blart")
            msg.delete().catch(_ => {});
    }
}
module.exports = [
    {
        event: "messageCreate",
        name: "PaulBlart",
        func: paulBlart
    },
    {
        event: "messageUpdate",
        name: "PaulBlart2",
        func: paulBlart2
    }
];